package com.rlax.k8sdemo;

import com.rlax.corebin.launch.listener.CorebinApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 用户服务服务启动类
 * @author Rlax
 *
 */
@SpringBootApplication
public class DemoApplication {

    public static final String APPLICATION_NAME = "corebin-k8s-demo";

    public static void main(String[] args) {
        CorebinApplication.run(APPLICATION_NAME, DemoApplication.class, args);
    }

}
