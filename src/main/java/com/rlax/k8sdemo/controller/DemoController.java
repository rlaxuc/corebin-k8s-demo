package com.rlax.k8sdemo.controller;

import cn.hutool.core.date.DateTime;
import com.rlax.corebin.boot.web.base.BaseController;
import com.rlax.corebin.core.result.R;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * demo 控制器
 *
 * @author Rlax
 * @since 2021-03-31
 */
@RestController
@AllArgsConstructor
public class DemoController extends BaseController {

	@GetMapping("/hello")
	public R<String> hello() {
		return R.success("hello world!");
	}

	@GetMapping("/echo")
	public R<String> echo(@RequestParam String echo) {
		return R.success(echo);
	}

	@GetMapping("/date")
	public R<String> date() {
		return R.success(DateTime.now().toDateStr());
	}
}
