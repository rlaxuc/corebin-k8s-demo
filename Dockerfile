FROM java:8-jdk-alpine
MAINTAINER rlaxuc <rlaxuc@gmail.com>

ARG JAR_FILE
COPY ${JAR_FILE} /app.jar

ENTRYPOINT ["java", "-Xmx512m", "-Djava.security.egd=file:/dev/./urandom", "-Ddruid.mysql.usePingMethod=false", "-jar", "/app.jar"]
CMD ["--spring.profiles.active=dev"]

# docker build --build-arg JAR_FILE=target/corebin-k8s-demo.jar -t corebin-k8s-demo:v1 .
# docker run -idt  --name corebin-k8s-demo --restart=always -p 8080:8080  corebin-k8s-demo:v1
